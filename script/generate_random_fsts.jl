using ArgParse
using MachineZoo
using CSV
using JSON

function parse_commandline()
    s = ArgParseSettings()
    # s2 = ArgParseSettings()


    @add_arg_table s begin
        "--output", "-o"
        help = "output file"
        arg_type = String
        required = true
        "--nmachines", "-m"
        help = "number of machines"
        arg_type = Int
        default = 1
        "--config", "-c"
        help = "config file"
        arg_type = String
    end

    add_arg_group(s, "machine types");

    @add_arg_table s begin
        "random_fst"
        action = :command
        help = "generate random FSTs"
        "dense_random_fst"
        action = :command
        help = "generate dense random FSTs"
        "complete_acyclic_fst"
        action = :command
        help = "generate complete acyclic FSTs"
        "topology_fst"
        action = :command
        help = "generate topology FSTs"
    end

    @add_arg_table s["random_fst"] begin
        "--acceptor", "-a"
        help = "acceptor"
        arg_type = Bool
        default = false
        "--acyclic", "-c"
        help = "acyclic"
        arg_type = Bool
        default = false
        "--semiring", "-s"
        help = "semiring"
        arg_type = String
        default = "tropical"
        "--nstates", "-n"
        help = "number of states"
        arg_type = Int
        default = 10
        "--nsymbols", "-y"
        help = "number of symbols"
        arg_type = Int
        default = 10
        "--arc_density", "-d"
        help = "arc density"
        arg_type = Float64
        default = 0.01
        "--narcs", "-r"
        help = "number of arcs"
        arg_type = Int
        default = 10
        "--seed"
        help = "seed"
        arg_type = Int
        default = 123456
    end

    @add_arg_table s["dense_random_fst"] begin
        "--semiring", "-s"
        help = "semiring"
        arg_type = String
        default = "tropical"
        "--nstates", "-n"
        help = "number of states"
        arg_type = Int
        default = 10
        "--nsymbols", "-y"
        help = "number of symbols"
        arg_type = Int
        default = 10
        "--seed"
        help = "seed"
        arg_type = Int
        default = 123456
    end

    @add_arg_table s["complete_acyclic_fst"] begin
        "--semiring", "-s"
        help = "semiring"
        arg_type = String
        default = "tropical"
        "--nstates", "-n"
        help = "number of states"
        arg_type = Int
        default = 10
        "--nsymbols", "-y"
        help = "number of symbols"
        arg_type = Int
        default = 10
        "--seed"
        help = "seed"
        arg_type = Int
        default = 123456
    end
    @add_arg_table s["topology_fst"] begin
        "--semiring", "-s"
        help = "semiring"
        arg_type = String
        default = "tropical"
        "--nsymbols", "-y"
        help = "number of symbols"
        arg_type = Int
        "--seed"
        help = "seed"
        arg_type = Int
        default = 123456
    end
    parse_args(ARGS, s)
end

args = parse_commandline()

# println(args)

params = args[args["%COMMAND%"]]
params["machine_type"] = args["%COMMAND%"]

if haskey(args, "config")
    config_file = args["config"]
    d = JSON.parsefile(config_file)
    if d["machine_type"] != args["%COMMAND%"]
        error("Machine type in config file does not match command")
    end
    p = d["params"]
    merge!(p, params)
    params = p
end

println(params)

if args["%COMMAND%"] == "random_fst"
    generator = random_fst
elseif args["%COMMAND%"] == "dense_random_fst"
    generator = dense_random_fst
elseif args["%COMMAND%"] == "complete_acyclic_fst"
    generator = complete_acyclic_fst
elseif args["%COMMAND%"] == "topology_fst"
    generator = topology_fst
else
    error("Unknown machine type")
end

# create directory if it does not exist
if !isdir(args["output"])
    mkpath(args["output"])
end

call_generator(generator, args["nmachines"], args["output"], params)
