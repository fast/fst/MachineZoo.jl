JULIA_ENV=./
export LD_LIBRARY_PATH=../OpenFst.jl/src/:../OpenFst.jl/openfst-1.8.3/src/lib/ 

# if directory machines/dense does not exist, create it

if [ ! -d "machines/dense" ]; then
    mkdir -p machines/dense
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/dense/tropical -m 1 -c script/config/dense_random_fst.json dense_random_fst --nstates 10 --nsymbols 10 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/dense/tropical -m 1 -c script/config/dense_random_fst.json dense_random_fst --nstates 100 --nsymbols 10 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/dense/tropical -m 1 -c script/config/dense_random_fst.json dense_random_fst --nstates 1000 --nsymbols 10 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/dense/tropical -m 1 -c script/config/dense_random_fst.json dense_random_fst --nstates 10 --nsymbols 100 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/dense/tropical -m 1 -c script/config/dense_random_fst.json dense_random_fst --nstates 100 --nsymbols 100 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/dense/tropical -m 1 -c script/config/dense_random_fst.json dense_random_fst --nstates 1000 --nsymbols 100 --seed 1000
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/dense/tropical

    for file in machines/dense/tropical/*.fst; do
        out=${file/tropical/log}
        dir=$(dirname "$out")
        mkdir -p $dir
        fstmap -map_type="to_log" $file $out
    done
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/dense/log
fi

if [ ! -d "machines/random" ]; then
    mkdir -p machines/random

    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 10 --nsymbols 10 --narcs 100 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 100 --nsymbols 10 --narcs 1000 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 1000 --nsymbols 10 --narcs 10000 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 10 --nsymbols 100 --narcs 100 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 100 --nsymbols 100 --narcs 1000 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 1000 --nsymbols 100 --narcs 10000 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/random/tropical -m 5 -c script/config/random_fst.json random_fst --nstates 1000 --nsymbols 100 --narcs 100000 --seed 1000
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/random/tropical

    for file in machines/random/tropical/*.fst; do
        out=${file/tropical/log}
        dir=$(dirname "$out")
        mkdir -p $dir
        fstmap -map_type="to_log" $file $out
    done
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/random/log
fi

if [ ! -d "machines/acyclic" ]; then
    mkdir -p machines/acyclic
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/complete_acyclic/tropical -m 1 -c script/config/complete_acyclic_fst.json complete_acyclic_fst --nstates 10 --nsymbols 10 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/complete_acyclic/tropical -m 1 -c script/config/complete_acyclic_fst.json complete_acyclic_fst --nstates 100 --nsymbols 10 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/complete_acyclic/tropical -m 1 -c script/config/complete_acyclic_fst.json complete_acyclic_fst --nstates 10 --nsymbols 50 --seed 1000
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/complete_acyclic/tropical -m 1 -c script/config/complete_acyclic_fst.json complete_acyclic_fst --nstates 100 --nsymbols 50 --seed 1000
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/complete_acyclic/tropical 

    for file in machines/complete\_acyclic/tropical/*.fst; do
        out=${file/tropical/log}
        dir=$(dirname "$out")
        mkdir -p $dir
        fstmap -map_type="to_log" $file $out
    done
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/complete_acyclic/log
fi


if [ ! -d "machines/topology" ]; then
    mkdir -p machines/topology
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/topology/tropical -c script/config/topology.json topology_fst --nsymbols 10
    julia --project=$JULIA_ENV script/generate_random_fsts.jl -o machines/topology/tropical -c script/config/topology.json topology_fst --nsymbols 100
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/topology/tropical 

    for file in machines/topology/tropical/*.fst; do
        out=${file/tropical/log}
        dir=$(dirname "$out")
        mkdir -p $dir
        fstmap -map_type="to_log" $file $out
    done
    julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/topology/log
fi


# julia --project=$JULIA_ENV script/txt2charlmfst.jl -i script/charlms/wotw.txt -n 5
# python3 -m kaldilm --read-symbol-table="script/charlms/symbols.txt" --disambig-symbol='#0' "script/charlms/charwotw.5.arpa" > "script/charlms/charwotw.5.fst.txt"
# fstcompile script/charlms/charwotw.5.fst.txt script/charlms/charwotw.5.fst
# cp script/charlms/charwotw.5.fst machines/charlm/tropical/charwotw.5.fst
# fstcompile --arc_type=log script/charlms/charwotw.5.fst.txt script/charlms/charwotw.5.fst
# cp script/charlms/charwotw.5.fst machines/charlm/log/charwotw.5.fst

# julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/charlm/tropical
# julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/charlm/log

for file in machines/composition/tropical/*.fst; do
    out=${file/tropical/log}
    dir=$(dirname "$out")
    mkdir -p $dir
    fstmap -map_type="to_log" $file $out
done

julia --project=$JULIA_ENV script/dir2fstinfo.jl -d machines/composition/tropical