using ArgParse
using MachineZoo
using CSV

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--input_text", "-i"
        arg_type = String
        required = true
        "--order", "-n"
        help = "order of the model"
        arg_type = Int
        required = true        
    end
    parse_args(ARGS, s)
end

args = parse_commandline()

txt2charlm(args["input_text"], args["order"])