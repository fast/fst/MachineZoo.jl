using Glob
import Unicode

function call_generator(generator, n_machines, output_path, params)

    p = params    

    if p["semiring"] == "tropical"
        S = SR.TropicalSemiring{Float32}
    elseif p["semiring"] == "log"
        S = SR.LogSemiring{Float32,-1}
    end
    
    offset = [parse(Int,split(splitpath(x)[end],"-")[1]) for x in glob("$(output_path)/*.fst")]
    offset = length(offset) == 0 ? 0 : maximum(offset)

    if p["machine_type"] == "topology_fst"
        n_machines = 1
    end

    for num in 1:n_machines

        nseed = p["seed"] + num
        onum = offset + num

        if p["machine_type"] == "topology_fst"            
            filename = "$(output_path)/$(lpad(onum,4,"0"))-.fst"
        else
            filename = "$(output_path)/$(lpad(onum,4,"0"))-seed_$(nseed).fst"
        end

        if isfile(filename)
            continue
        end

        if p["machine_type"] == "random_fst"
            machine = generator(S, p["nstates"], p["nsymbols"], p["arc_density"];narcs=p["narcs"],
                acceptor=p["acceptor"], acyclic=p["acyclic"], seed=nseed)

        elseif p["machine_type"] == "dense_random_fst"
            machine = generator(S, p["nstates"], p["nsymbols"]; seed=nseed)
        elseif p["machine_type"] == "complete_acyclic_fst"
            machine = generator(S, p["nstates"], p["nsymbols"]; seed=nseed)
        elseif p["machine_type"] == "topology_fst"
            symbols = 1:p["nsymbols"]
            initweights = Dict(parse(Int,string(k))=>v  for (k,v) in pairs(p["initweights"]))
            finalweights = Dict(parse(Int,string(k))=>v  for (k,v) in pairs(p["finalweights"]))
            machine = generator(S, [Tuple(x) for x in p["topology"]], initweights, finalweights, symbols)
            # println(machine)
        else
            error("Unknown machine type")
        end
        
        OF.write(OF.arcsort(OF.VectorFst(machine),OF.:ilabel), filename)
        
    end

end

function execute(cmd::Cmd)
    out = Pipe()
    err = Pipe()

    process = run(pipeline(ignorestatus(cmd), stdout=out, stderr=err))
    close(out.in)
    close(err.in)

    (
        stdout=String(read(out)),
        stderr=String(read(err)),
        code=process.exitcode
    )
end

function fstinfo(file)
    records = []
    s = execute(`fstinfo $(file)`).stdout
    for item in split(s, "\n")
        if length(item) > 1
            push!(records, Tuple(split(item, r"\s\s+")))
        end
    end
    if length(records) == 0
        return nothing
    end
    df = DataFrame(records, [:prop, :val])
    insertcols!(df, :file => file)
end


"""
txtformat4charlm(s::String)

Converts input string to a format suitable for training character-level language models.

spaces are replaced by <pad>
newlines are replaced by <s> <\\s>
"""
function txtformat4charlm(s::String)
    s = Unicode.normalize(s, stripmark=true)
	s = replace(s, r"([A-Z])\.\s" => s"\1 ")
	s = replace(s, r"\. " => "@")
	s = replace(s, r"\r\n\r\n" => "@")
	s = lowercase(s)
	s = replace(s, r"\n" => " ")
	s = replace(s, r"\r" => " ")
	s = replace(s, r"[^a-z@'\s]" => "")	
	s = replace(s, r"\s\s+" => "")
	s = replace(s, r"(@\s*)+" => "@")
	s = split(s,"")
	s = replace(s," "=>"<pad>")
	s = join(s," ")
	s = replace(s, r"\<pad\> @" => "\n")
	s = replace(s, "@ " => "\n")
	# s = replace(s, r"\@\s*" => "\n")
	s
end

"""
txt2charlm(file::String, order::Int)

Reads a text file and trains a character-level language model using kernlm's lmplz, and saves the model in ARPA format.
"""
function txt2charlm(file::String, order::Int)
	s = open(file) do file
		read(file, String)
	end
	filename = splitpath(file)[end]
	charfilename = replace(file,filename=>"char"*filename)
	arpafilename = replace(charfilename,".txt"=>".$(order).arpa")
	s = txtformat4charlm(s)
	open(charfilename,"w") do file
	    write(file, s)
	end
	out = execute(`lmplz -o $(order) --discount_fallback --text $(charfilename) --arpa $(arpafilename)`)
    print(out.stdout)
    print(out.stderr)
end